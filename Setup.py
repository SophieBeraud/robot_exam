from setuptools import setup

setup (
    name="Exam",
    version='0.0.1',
    author="Sophie Béraud",
    description="Jeu deplacement de robot",
    license="GNU GPLv3",
    python_requires='>=2.4',
    packages=['Robot','test'],
)
