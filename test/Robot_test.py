#!usr/bin/env python 
#coding : utf-8

"""Classe de test"""

import unittest
import logging
from Robot import Robot

#test
class TestAddPosition(unittest.TestCase):
    "Classe de test"
    logging.warning("Debut test methode add Position")

    def setUp(self):
        "init"
        self.jeu=Robot.Grid()
    
    def test_add_ok(self):
        "Test ok ajout"
        self.jeu.addPosition(self,1,1,1,4)
        objectifdico = {1: (1,1,4)}
        self.assertEqual(self.__dico.get(1), objectifdico.get(1))

    def test_add_not_ok(self):
        "Test not ok ajout"
        self.jeu.addPosition(self,1,1,1,4) # deja un id 1 , le test devrai etre faux
        objectifdico = {2: (2,1,4)}
        self.assertNotEqual(self.__dico.get(1), objectifdico.get(1))

class TestRemovePosition(unittest.TestCase):
    "Classe de test"
    logging.warning("Debut test methode remove Position")

    def setUp(self):
        "init"
        self.jeu=Robot.Grid()
    
    def test_remove_ok(self):
        "Test ok ajout"
        self.jeu.removePosition(self, 1)
 
class TestListePostion(unittest.TestCase):
    "Classe de test"
    logging.warning("Debut test methode liste Position")

    def setUp(self):
        "init"
        self.jeu=Robot.Grid()

class TestPostion(unittest.TestCase):
    "Classe de test"
    logging.warning("Debut test methode Position")

    def setUp(self):
        "init"
        self.jeu=Robot.Grid()


if __name__ == "__main__":
    unittest.main()

