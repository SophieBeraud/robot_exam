##!usr/bin/env python 
#coding : utf-8

import math 

""" Classe permettant de créer un ensemble de position """
class Grid:

    """ Constructeur d'initialisation"""
    def __init__(self, identifiant, positionx, positiony, rayon):
        self.__id= identifiant  #identifiant unique on le met alors en private afin que personne ne le modifie
        self.__x = positionx
        self.__y = positiony
        self.__r = 2*rayon*3,14
        self.__dico = {}
        self.liste = []
        self.x1 = ()
        self.y1 = ()
        self.x2 = ()
        self.y2 = ()
        self.collision = 0 # 0 pas de collision, on aurait pu faire un boolean 
        self.r1 = 0
        self.r2 = 0
        self.tupleposition = ()
        self.dist = 0
    
    def addPosition(self, id1, x, y, r):
        """Ajouter une postion"""
        self.__dico[id1] =(x, y, r)
        return self.__dico

    def removePosition(self, id1):
        """Retirer une position"""
         # si le dico possede un position correspondant e la cle il faudrai verifier avec haskey
        del self.__dico[id1] # on supprime l'entrée en indiquant la cle, ici l'id qui est unique
        return self.__dico
    
    def listePosition(self):
        """Lister les positions """
         #initilialisation d'une liste vide
        for position in self.__dico.values(): #On parcourt tout les positions qui correspondent au valeur garce a la méthode values
            self.liste.append(position) # on ajoute a la liste les tuples de position
            print (self.liste)
        return self.liste

    def position(self, identi):
        """Recuperer une  position """
        #if (self.__dico.has_key(id)):
        return self.__dico.get(identi)
        #else: 
            #return
    
    def distance(self, id1, id2):
        """Calculer la distance euclidienne entre 2 positions"""
        #Recuperation de la valeur(tuple) correspondant a l id1
        self.tupleposition =self.__dico.get(id1)
        self.x1 =self.tupleposition[0] #recuperation de x1 
        self.y1 =self.tupleposition[1] #recuperation de y1
        #Recuperation de la valeur(tuple) correspondant a l id2
        self.tupleposition=self.__dico.get(id2)
        self.x2 =self.tupleposition[0] #recuperation de x2 
        self.y2 =self.tupleposition[1] #recuperation de y2
        #calcul de la distance
        self.dist = math.sqrt((self.x2 - self.x1) * (self.x2 -self.x1) + (self.y2 - self.y1) * (self.y2 - self.y1))
        return self.dist

    def colision(self, id1, id2):
        """ Permet de calculer la collision """
        self.tupleposition =self.__dico.get(id1)
        self.r1 = self.tupleposition[3] #recuperation de r1 
        self.tupleposition =self.__dico.get(id2)
        self.r2 = self.tupleposition[3] #recuperation de r2 
        #distance e deux cerclesest < à la somme de leur rayons
        return self.collision
